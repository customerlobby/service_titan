require 'spec_helper'

RSpec.describe ServiceTitan::Client::Invoices do
  before do
   @client = ServiceTitan::Client.new({api_key: "TEST"})
  end

  describe '#invoices' do
   it 'should return a list of orders' do
     stub_get("invoices").to_return(body: fixture('invoices_list.json'), :headers => {:content_type => "application/json; charset=utf-8", authorization: 'Basic blah'})

     @client.invoices
     expect(a_get("invoices")).to have_been_made
   end
  end

end
