require 'spec_helper'

RSpec.describe ServiceTitan::Client::Customers do
  before do
    @client = ServiceTitan::Client.new({api_key: "TEST"})
  end

  describe '#customers' do

   before do
     stub_get("customers").to_return(body: fixture('customers_list.json'), :headers => {:content_type => "application/json; charset=utf-8", authorization: 'blah'})
   end

   it 'should return a list of customers' do
     @client.customers
     expect(a_get("customers")).to have_been_made
   end
  end

end
