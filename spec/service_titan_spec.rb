require 'spec_helper'

RSpec.describe ServiceTitan do
  after do
    ServiceTitan.reset
  end

  describe ".client" do
    it "should be a ServiceTitan::Client" do
      expect(ServiceTitan.client).to be_a(ServiceTitan::Client)
    end
  end

  describe '#api_key' do
    it 'should return the default api key' do
      expect(ServiceTitan.api_key).to eq(ServiceTitan::Configuration::DEFAULT_API_KEY)
    end
  end

  describe '#api_key=' do
    it 'should set the api key' do
      ServiceTitan.api_key = 'test'
      expect(ServiceTitan.api_key).to eq('test')
    end
  end

  describe '#api_version' do
    it 'should return the default api version' do
      expect(ServiceTitan.api_version).to eq(ServiceTitan::Configuration::DEFAULT_API_VERSION)
    end
  end

  describe '#api_version=' do
    it 'should set the api_version' do
      ServiceTitan.api_version = '/test'
      expect(ServiceTitan.api_version).to eq('/test')
    end
  end

  describe '#adapter' do
    it 'should return the default adapter' do
      expect(ServiceTitan.adapter).to eq(ServiceTitan::Configuration::DEFAULT_ADAPTER)
    end
  end

  describe '#adapter=' do
    it 'should set the adapter' do
      ServiceTitan.adapter = :typhoeus
      expect(ServiceTitan.adapter).to eq(:typhoeus)
    end
  end

  describe '#endpoint' do
    it 'should return the default endpoint' do
      expect(ServiceTitan.endpoint).to eq(ServiceTitan::Configuration::DEFAULT_ENDPOINT)
    end
  end

  describe '#endpoint=' do
    it 'should set the endpoint' do
      ServiceTitan.endpoint = 'http://www.google.com'
      expect(ServiceTitan.endpoint).to eq('http://www.google.com')
    end
  end

  describe '#configure' do
    ServiceTitan::Configuration::VALID_OPTIONS_KEYS.each do |key|

      it "should set the #{key}" do
        ServiceTitan.configure do |config|
          config.send("#{key}=", key)
          expect(ServiceTitan.send(key)).to eq(key)
        end
      end
    end
  end
end
