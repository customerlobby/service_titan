require 'faraday'
require 'faraday_middleware'
require 'active_support/all'
require 'service_titan/version'

Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}
require File.expand_path('../service_titan/configuration', __FILE__)
require File.expand_path('../service_titan/api', __FILE__)
require File.expand_path('../service_titan/client', __FILE__)
require File.expand_path('../service_titan/error', __FILE__)

module ServiceTitan

  extend Configuration
  # Alias for ServiceTitan::Client.new
  # @return [ServiceTitan::Client]
  def self.client(options = {})
    ServiceTitan::Client.new(options)
  end

  # Delegate to ServiceTitan::Client
  def self.method_missing(method, *args, &block)
    return super unless client.respond_to?(method)
    client.send(method, *args, &block)
  end
end
