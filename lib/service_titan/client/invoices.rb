module ServiceTitan
  class Client
    module Invoices

      def invoices(params = {})
        get("invoices", params)
      end

      def invoice(id, params = {})
        get("invoices/#{id}", params)
      end

    end
  end
end
