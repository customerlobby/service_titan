module ServiceTitan
  class Client
    module Jobs

      def jobs(params = {})
        get("jobs", params)
      end

      def job(id, params = {})
        get("jobs/#{id}", params)
      end

    end
  end
end
