module ServiceTitan
  # Wrapper for the ServiceTitan REST API.
  class Client < API
    Dir[File.expand_path('../client/*.rb', __FILE__)].each{|f| require f}

    include ServiceTitan::Client::Customers
    include ServiceTitan::Client::Invoices
    include ServiceTitan::Client::Jobs
  end
end
