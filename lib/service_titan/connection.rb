require 'faraday_middleware'
Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}

module ServiceTitan
  module Connection
    private

    def connection
      options = {
        :url => "#{endpoint}#{api_version}/"
      }
      
      Faraday::Connection.new(options) do |connection|
        connection.use FaradayMiddleware::ServiceTitanAuth, api_key
        connection.use FaradayMiddleware::Mashify
        connection.use Faraday::Response::ParseJson
        connection.adapter(adapter)
        connection.use Faraday::Response::Logger, logger if logger
      end
    end
  end
end
